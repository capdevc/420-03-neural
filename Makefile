CXX = g++
CFLAGS = -Wall -O2 -g -fopenmp -std=c++0x
LFLAGS = -lmgl -fopenmp

all:	hopfield


hopfield:	hopfield.o
	$(CXX) hopfield.o $(LFLAGS) -o hopfield

hopfield.o:	hopfield.cpp hopfield.h
	$(CXX) $(CFLAGS) -c hopfield.cpp

clean:	
	rm -f *~ *.o hopfield


