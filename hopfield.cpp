#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <mgl/mgl_zb.h>
#include "hopfield.h"
using namespace std;

// HNet constructor generates Patterns and stores them in an array
// It also initializes some data storage arrays
HNet::HNet()
{
    for (size_t i = 0; i < Patterns; ++i) {
        patterns[i] = new Pattern;
        stable[i] = avg_flipped_bits[i] = pstable[i] = 0;
    }
    for (size_t i = 0; i < Neurons; ++i)
        for (size_t j = 0; j < Neurons; ++j )
            weights[i][j] = 0.0;
    patterns_imprinted = 0;
}

// HNet destructor frees the pattern storage memory
HNet::~HNet()
{
    for (size_t i = 0; i < Patterns; ++i) {
        delete patterns[i];
    }
}

// Take a particular pattern in the pattern array and imprint it onto
// the neurons
void HNet::imprint_pattern(const int p)
{
    for (size_t j = 0; j < Neurons; ++j)
        for (size_t k = 0; k < j; ++k) {
            weights[j][k] += ((double)((*patterns[p])[j] * (*patterns[p])[k])
                              / (double)Neurons);
            weights[k][j] = weights[j][k];
        }
    for (size_t i = 0; i < Neurons; ++i)
        weights[i][i] = 0;
    patterns_imprinted++; // track how many we have imprinted
}

// generate the fields array showing what the new state should be based
// on the patterns imprinted and the current state of the neurons
void HNet::calc_fields()
{
    for (size_t i = 0; i < Neurons; ++i) {
        double sum = 0;
        for (size_t j = 0; j < Neurons; ++j) {
            sum += weights[i][j] * neurons[j];
        }
        fields[i] = ((sum >= 0) ? POS : NEG);
    }
}

// set the neurons to one of the patterns from the pattern array and
// see if it's stable. returns an int showing the number of bits that
// want to flip.
int HNet::test_pattern(const int p)
{
    // copy the pattern to be tested to the neurons
    memcpy(neurons, patterns[p], sizeof(state_t) * Neurons);
    // caculate the fields
    calc_fields();
    int bad_bits = 0;
    for (size_t i = 0; i < Neurons; ++i)
        // this checks for a flipped bit without a conditional
        bad_bits -= (neurons[i] == fields[i]) - 1;
    if (!bad_bits) {
        stable[patterns_imprinted-1]++; // count this as a stable pattern
        pstable[patterns_imprinted-1] += 1.0 / patterns_imprinted;
    }
    avg_flipped_bits[patterns_imprinted-1] += bad_bits / (double) patterns_imprinted;
    return bad_bits;
}

const double HNet::get_stable(const int patterns) const
{
    return stable[patterns];
}

const double HNet::get_flipped_bits(const int patterns) const
{
    return avg_flipped_bits[patterns];
}

// a little print routine for debugging
void HNet::print() const
{
    //for (size_t i = 0; i < Neurons; ++i)
    //cout << i << " :\t" << neurons[i] << "\t" << fields[i] << "\t"
    //<< (*patterns[0])[i] << endl;
    //for (size_t i = 0; i < Neurons; ++i) {
    //for (size_t j = 0; j < Neurons; ++j)
    //cout << " " << weights[i][j];
    //cout << endl;
    //}
    for (size_t i = 0; i < Patterns; ++i)
        cout << i+1 <<":\t" << setprecision(3) << avg_flipped_bits[i] << "\t"
             << stable[i] << "\t" << pstable[i] << endl;
}

// the super ugly plot function which generates the graphs.
// takes an array of stable pattern counts and unstable pattern counts
// indexed by the number of patterns stored
void plot_graphs(double *avg_stable, double *avg_unstable, double *avg_flipped_bits) {
    double xs[Patterns], unstable_percent[Patterns];

    mglGraphZB gr1(600, 600), gr2(600, 600), gr3(600, 600);
    mglData x;
    for (uint i = 0; i < Patterns; ++i)
        xs[i] = i+1;
    x.Set(xs, Patterns);

    for (size_t i = 0; i < Patterns; ++i)
        unstable_percent[i] = avg_unstable[i]/(i+1);
    mglData y0;
    y0.Set(unstable_percent, Patterns);
    gr1.SetRanges(0, Patterns, 0, Patterns);
    gr1.YRange(y0);
    gr1.SetFontSizeCM(0.4);
    gr1.SetTicks('x', Patterns/5, 0, Patterns/5);
    gr1.SetTicks('y', 0.1, 0, 0.1);
    gr1.Axis("xy");
    gr1.Title("Percentage of Unstable Patterns");
    gr1.SetFontSizeCM(0.5);
    gr1.Label('x', "Patterns Stored");
    gr1.Label('y', "% Unstable");
    gr1.Plot(x, y0, "r");
    gr1.SetFontSizeCM(0.5);
    gr1.WritePNG("graph1.png", "", false);

    y0.Set(avg_stable, Patterns);
    gr2.SetRanges(0, Patterns, 1, Patterns);
    gr2.YRange(y0);
    gr2.SetFontSizeCM(0.4);
    gr2.SetTicks('x', Patterns/5, 0, Patterns/5);
    gr2.SetTicks('y', Patterns/25, 0, Patterns/25);
    gr2.Axis("xy");
    gr2.Title("Average Stable Patterns");
    gr2.SetFontSizeCM(0.5);
    gr2.Label('x', "Patterns Stored");
    gr2.Label('y', "Avg. Patterns Stable");
    gr2.Plot(x, y0, "g");
    gr2.SetFontSizeCM(0.5);
    gr2.WritePNG("graph2.png", "", false);

    y0.Set(avg_flipped_bits, Patterns);
    gr3.SetRanges(0, Patterns, 1, Patterns);
    gr3.YRange(y0);
    gr3.SetFontSizeCM(0.4);
    gr3.SetTicks('x', Patterns/5, 0, Patterns/5);
    gr3.SetTicks('y', Patterns/50, 0, Patterns/50);
    gr3.Axis("xy");
    gr3.Title("Average Bits Inorrect");
    gr3.SetFontSizeCM(0.5);
    gr3.Label('x', "Patterns Stored");
    gr3.Label('y', "Bits Incorrect");
    gr3.Plot(x, y0, "b");
    gr3.SetFontSizeCM(0.5);
    gr3.WritePNG("graph3.png", "", false);
}

int main(int argc, char const *argv[])
{
    srand(time(NULL));
    int runs = 200;
    double avg_stable[Patterns], avg_unstable[Patterns], avg_flipped_bits[Patterns];

    for (size_t i = 0; i < Patterns; ++i)
        avg_stable[i] = avg_unstable[i] = avg_flipped_bits[i] = 0;

    // use simple openMP multithreading to run these trials in parallel
    // This is a pretty simple reduction over a for loop
#pragma omp parallel for shared(avg_stable)
    for (int run = 0; run < runs; ++run) {
        HNet hn;
        for (size_t i = 0; i < Patterns; ++i) {
            hn.imprint_pattern(i);
            for (size_t j = 0; j <= i; ++j)
                hn.test_pattern(j);
            avg_stable[i] += hn.get_stable(i) / (double) runs;
            avg_flipped_bits[i] += hn.get_flipped_bits(i) / (double) runs;
        }
    }

    for (size_t i = 1; i <= Patterns; ++i)
        avg_unstable[i-1] = i - avg_stable[i-1];

    plot_graphs(avg_stable, avg_unstable, avg_flipped_bits);
    return 0;
}
