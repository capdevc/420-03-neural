#ifndef HOPFIELD_H
#define HOPFIELD_H

#include <iostream>
#include <iomanip>
#include <stdlib.h>
using namespace std;

const size_t Neurons = 200;
const size_t Patterns = 100;
enum state_t {NEG = -1, POS = 1};

// pattern class for holding a single pattern
class Pattern {
public:
    Pattern();
    ~Pattern();
    state_t &operator[](const size_t index) {
        return bits[index];
    }
private:
    state_t bits[Neurons];
};

// constructor generates a random pattern
Pattern::Pattern()
{
    for (size_t i = 0; i < Neurons; ++i) {
        bits[i] = (rand() % 2) ? POS : NEG;
    }
}

Pattern::~Pattern()
{
}

class HNet {
public:
    HNet();
    ~HNet();
    void imprint_pattern(const int p);
    void calc_fields();
    int test_pattern(const int p);
    const double get_stable(const int patterns) const;
    const double get_flipped_bits(const int patterns) const;
    void print() const;

private:
    state_t neurons[Neurons]; // current states of neurons
    state_t fields[Neurons]; // current field on neurons
    double weights[Neurons][Neurons]; // connection weights
    double avg_flipped_bits[Patterns]; // avg bits that want to flip
    double pstable[Patterns]; // percent stable, indexed by # patterns stored
    Pattern *patterns[Patterns]; // Array of patterns generated
    int patterns_imprinted; // total patterns actually stored in network
    int stable[Patterns]; // count of stable patterns, indexed by # stored
};


#endif /* end of include guard: HOPFIELD_H */

